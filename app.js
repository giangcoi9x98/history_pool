require('dotenv').config()
const express = require("express")
const app = express()
const cors = require('cors')
const historyModel = require('./Models/poolHistory')
const web3 = require('./config/web3Connect')
// socket
const http = require('http');
const server = http.createServer(app);
const io = require("socket.io");

app.use(cors())

async function init() {
  await web3.eth.subscribe('newBlockHeaders', function (error, result) {
    if (!error) {
      console.log(result);
      return;
    }
    console.error(error);
  })
    .on('connected', function (subscriptionId) {
    })
    .on('data', async (blockHeader) => {
    })
    .on('error', console.error);

  await web3.eth.subscribe(
    'logs',
    {
      address: "0xd302797a732A184F9608AE5a0Ec7730567AB46b8",
    },
    (error) => {
      if (error) {
        return console.error(error);
      }
    }
  )
    .on('connected', (subscriptionId) => {
      console.log('subscriptionId: ' + subscriptionId);
    })
    .on('data', async (log) => {
      console.log('log', log);
      let action =
        log.topics[0] == '0x90890809c654f11d6e72a28fa60149770a0d11ec6c92319d6ceb2bb0a4ea1a15'
          ? 'Deposit'
          : 'Withdraw'
      const value = await web3.eth
        .getTransaction(log.transactionHash)
        .then(res => {
          if (res.length > 74) {
            return web3.utils.hexToNumber('0x' + res.input.substring(74, 138)).toString()
          } else {
            return web3.utils.toBN(res.input.substring(56, 74)).toString()
          }
        })
      socketInstance.emit("NEW_HISTORY", {
        action: action,
        address: log.address,
        amount: value,
        hash: log.transactionHash,
        time: Date.now()
      })
      await historyModel.create({
        action: action,
        address: log.address,
        amount: value,
        hash: log.transactionHash,
        time: Date.now()
      })
    })
    .on('changed', (log) => console.log(log));
}
init()
app.use('/getAllHistory', async function (req, res) {
  try {
    let data = await historyModel.find({}).sort({ time: -1 })
    if (data) {
      res.send({
        data: data.map(
          e => {
            return {
              action: e.action,
              address: e.address,
              amount: e.amount,
              hash: e.hash,
              time: e.time
            }
          }
        )
      })
    }
  } catch (error) {
    console.log(error);
  }
})

const socketServer = (function () {
  var instance;
  function init() {
    console.log('init socket server')
    instance = io(server, {
      cors: {
        origin: "*",
        methods: ["GET", "POST", "PUT", "DELETE"],
      }
    })
    return instance
  };
  return {
    getInstance: () => instance || init()
  };
})();

var socketInstance = socketServer.getInstance();
const PORT = process.env.PORT | 3456
server.listen(PORT, function () {
  console.log("Success connect at", PORT);
})