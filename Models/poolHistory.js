const mongoose = require("../config/dbConnect")
const HistorySchema = new mongoose.Schema({
    action: String,
    address: String,
    amount: String,
    hash: String,
    time: {
        type: String,
        default: Date.now()
    }
},{
    collection:"poolHitory"
})

const history = mongoose.model("poolHitory", HistorySchema)
module.exports = history