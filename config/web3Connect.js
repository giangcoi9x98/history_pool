const Web3 = require('web3');
const options = {
  reconnect: {
      auto: true,
      delay: 1000, 
      maxAttempts: 5,
      onTimeout: false
  }
};
var web3 = new Web3('wss://bsc-ws-node.nariox.org:443', options);

module.exports =  web3
